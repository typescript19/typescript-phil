// const express = require('express');

// import express from 'express';
import { default as express } from 'express';
import { default as morgan } from 'morgan';
import { default as cookieParser } from 'cookie-parser';
import * as auth from './auth';
import dotenv from 'dotenv';
const app = express();


// middleware
dotenv.config();
app.use(morgan('dev'));
app.use(express.json());
app.use(cookieParser());

// routes
app.get('/', auth.index);
app.post('/login', auth.login);
app.get('/logout', auth.logout);

app.listen(process.env.PORT);
// app.get('/', (req, res) => {
//   console.log('hello');
//   res.send('hello');
// });
// app.listen(3000);

// import dotenv from 'dotenv';
// dotenv.config();
// console.log(process.env);
// console.log('port', process.env.PORT);