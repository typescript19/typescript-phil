import { Request, Response } from 'express';

export const index = (req: Request, res: Response) => {
  if (req.cookies?.islogin === 'yes') {
    res.send('is login');
  } else {
    res.send('not login');
  }
};

export const login = (req: Request, res: Response) => {
  if (req.body.account === 'abc' && req.body.password === '123') {
    res.cookie('islogin', 'yes');
    res.json({ result: 'ok' });
    return;
  }

  res.json({ result: 'fail' });
};

export const logout = (req: Request, res: Response) => {
  res.clearCookie('islogin');
  res.send('logout');
};